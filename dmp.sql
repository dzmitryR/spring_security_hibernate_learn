--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.0
-- Dumped by pg_dump version 9.6.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: shl; Type: DATABASE; Schema: -; Owner: root
--

CREATE DATABASE shl WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';


ALTER DATABASE shl OWNER TO root;

\connect shl

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: s_human_id; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_human_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_human_id OWNER TO postgres;

--
-- Name: s_persistent_logins_id; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s_persistent_logins_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_persistent_logins_id OWNER TO postgres;

--
-- Name: s_user_id; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE s_user_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE s_user_id OWNER TO root;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: t_human; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_human (
    id numeric DEFAULT nextval('s_human_id'::regclass) NOT NULL,
    human_type text,
    name text,
    surname text
);


ALTER TABLE t_human OWNER TO postgres;

--
-- Name: t_persistent_logins; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_persistent_logins (
    username text,
    series text,
    token text,
    last_used timestamp without time zone,
    id numeric NOT NULL
);


ALTER TABLE t_persistent_logins OWNER TO postgres;

--
-- Name: t_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_role (
    id numeric NOT NULL,
    name text NOT NULL
);


ALTER TABLE t_role OWNER TO postgres;

--
-- Name: t_user; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE t_user (
    id numeric NOT NULL,
    name text NOT NULL,
    password text NOT NULL
);


ALTER TABLE t_user OWNER TO root;

--
-- Name: t_user_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE t_user_role (
    user_id numeric NOT NULL,
    role_id numeric NOT NULL
);


ALTER TABLE t_user_role OWNER TO postgres;

--
-- Name: s_human_id; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_human_id', 2, true);


--
-- Name: s_persistent_logins_id; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('s_persistent_logins_id', 9, true);


--
-- Name: s_user_id; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('s_user_id', 2, true);


--
-- Data for Name: t_human; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO t_human (id, human_type, name, surname) VALUES (1, 'AUT', 'Test', 'Surname');
INSERT INTO t_human (id, human_type, name, surname) VALUES (2, 'DIR', 'Vasia', 'Oblomob');


--
-- Data for Name: t_persistent_logins; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO t_persistent_logins (username, series, token, last_used, id) VALUES ('admin', 'eoiIH8e9LKYmO6GjOvwntw==', 'PJ2WR7tMtehshBTGEBxWcg==', '2017-02-12 23:54:07.4', 9);


--
-- Data for Name: t_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO t_role (id, name) VALUES (1, 'ROLE_USER');
INSERT INTO t_role (id, name) VALUES (2, 'ROLE_ADMIN');


--
-- Data for Name: t_user; Type: TABLE DATA; Schema: public; Owner: root
--

INSERT INTO t_user (id, name, password) VALUES (1, 'test1', 'pass');
INSERT INTO t_user (id, name, password) VALUES (2, 'admin', 'admin');


--
-- Data for Name: t_user_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO t_user_role (user_id, role_id) VALUES (2, 2);
INSERT INTO t_user_role (user_id, role_id) VALUES (1, 1);
INSERT INTO t_user_role (user_id, role_id) VALUES (2, 1);


--
-- Name: t_human t_human_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_human
    ADD CONSTRAINT t_human_pkey PRIMARY KEY (id);


--
-- Name: t_persistent_logins t_persistent_logins_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_persistent_logins
    ADD CONSTRAINT t_persistent_logins_pkey PRIMARY KEY (id);


--
-- Name: t_role t_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_role
    ADD CONSTRAINT t_role_pkey PRIMARY KEY (id);


--
-- Name: t_user t_user_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY t_user
    ADD CONSTRAINT t_user_pkey PRIMARY KEY (id);


--
-- Name: t_user_role f_role_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_user_role
    ADD CONSTRAINT f_role_id FOREIGN KEY (role_id) REFERENCES t_role(id);


--
-- Name: t_user_role f_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_user_role
    ADD CONSTRAINT f_user_id FOREIGN KEY (user_id) REFERENCES t_user(id);


--
-- PostgreSQL database dump complete
--

