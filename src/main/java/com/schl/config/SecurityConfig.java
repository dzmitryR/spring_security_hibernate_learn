package com.schl.config;

import com.schl.service.impl.PersistentTokenServiceImpl;
import com.schl.service.impl.UserDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  public static final int TOKEN_VALIDITY_SECONDS = 86400;

  private final UserDetailServiceImpl userDetailService;
  private final PersistentTokenServiceImpl persistentTokenService;

  @Autowired
  public SecurityConfig(UserDetailServiceImpl userDetailService,
                        PersistentTokenServiceImpl persistentTokenService) {
    this.userDetailService = userDetailService;
    this.persistentTokenService = persistentTokenService;
  }

  @Autowired
  public void registerGlobalAuthentication(AuthenticationManagerBuilder builder) throws Exception {
    builder.userDetailsService(userDetailService);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf()
        .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());

    http.authorizeRequests()
        .antMatchers("/static/**", "/**").permitAll()
        .anyRequest()
        .permitAll()
        .and();

    http.formLogin()
        .loginPage("/login")
        .loginProcessingUrl("/j_spring_security_check")
        .usernameParameter("j_username")
        .passwordParameter("j_password")
        .permitAll()
        .and()
        .rememberMe().key("rememberMeKey")
        .tokenRepository(persistentTokenService)
        .tokenValiditySeconds(TOKEN_VALIDITY_SECONDS)
    ;
  }
}
