package com.schl.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AdminController {

    @Secured(value = "ROLE_ADMIN")
    @RequestMapping(value = "/admin_page")
    public String redirectToAdminPage() {
        return "adminPage";
    }
}
