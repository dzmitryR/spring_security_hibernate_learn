package com.schl.controller;

import com.schl.entity.human.Human;
import com.schl.service.HumanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HumanController {

  private final HumanService humanService;

  @Autowired
  public HumanController(HumanService humanService) {
    this.humanService = humanService;
  }

  @RequestMapping(value = "/human/{name}", method = RequestMethod.GET)
  @ResponseBody
  public Human getHumanByName(@PathVariable("name") String name) {
    return humanService.findByName(name);
  }
}
