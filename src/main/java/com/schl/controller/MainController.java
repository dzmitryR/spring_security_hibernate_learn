package com.schl.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

    private final Logger logger = LoggerFactory.getLogger(MainController.class);

    @RequestMapping(value = "/")
    @Secured(value = {"ROLE_USER"})
    public String redirectToIndex() {
        return "index";
    }

    @RequestMapping(value = "/login")
    public String redirectToLogin() {
        return "login";
    }
}
