package com.schl.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Table(name = "t_persistent_logins")
public class PersistentLogin {

  @Id
  @GeneratedValue(generator = "s_persistent_logins_id")
  @SequenceGenerator(name = "s_persistent_logins_id", sequenceName = "s_persistent_logins_id", allocationSize = 1)
  private BigInteger id;

  @Column(name = "username")
  private String username;

  @Column(name = "series")
  private String series;

  @Column(name = "token")
  private String token;

  @Column(name = "last_used")
  private Date lastUsed;

  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getSeries() {
    return series;
  }

  public void setSeries(String series) {
    this.series = series;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Date getLastUsed() {
    return lastUsed;
  }

  public void setLastUsed(Date lastUsed) {
    this.lastUsed = lastUsed;
  }
}
