package com.schl.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

@Entity
@Table(name = "t_user")
public class User implements Serializable {
  @Id
  @GeneratedValue(generator = "s_user_id")
  @SequenceGenerator(name = "s_user_id", sequenceName = "s_user_id", allocationSize = 1)
  private BigInteger id;

  @Column(name = "name")
  private String name;

  @Column(name = "password")
  private String password;

  @ManyToMany(fetch = FetchType.EAGER,
      targetEntity = Role.class,
      cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  @JoinTable(name = "t_user_role",
      joinColumns = @JoinColumn(name = "user_id"),
      inverseJoinColumns = @JoinColumn(name = "role_id"))
  private List<Role> roles;

  public User() {
  }

  public User(String name, String password) {
    this.name = name;
    this.password = password;
  }

  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public List<Role> getRoles() {
    return roles;
  }

  public void setRoles(List<Role> roles) {
    this.roles = roles;
  }
}
