package com.schl.entity.converter;

import com.schl.entity.human.HumanType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class HumanTypeConverter implements AttributeConverter<HumanType, String> {

  @Override
  public String convertToDatabaseColumn(HumanType humanType) {
    if (humanType == null) {
      return null;
    } else {
      return humanType.getDatabaseType();
    }
  }

  @Override
  public HumanType convertToEntityAttribute(String s) {
    return HumanType.getByDatabaseType(s);
  }
}
