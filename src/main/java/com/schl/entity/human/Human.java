package com.schl.entity.human;

import com.schl.entity.converter.HumanTypeConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigInteger;

@Entity
@Table(name = "t_human")
public class Human {

  @Id
  @GeneratedValue(generator = "s_human_id")
  @SequenceGenerator(name = "s_human_id", sequenceName = "s_human_id", allocationSize = 1)
  private BigInteger id;
  @Column(name = "human_type")
  @Convert(converter = HumanTypeConverter.class)
  private HumanType humanType;
  @Column(name = "name")
  private String name;
  @Column(name = "surname")
  private String surname;

  public Human() {
  }

  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public HumanType getHumanType() {
    return humanType;
  }

  public void setHumanType(HumanType humanType) {
    this.humanType = humanType;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }
}
