package com.schl.entity.human;

public enum HumanType {

  AUTHOR("AUT"),
  DIRECTOR("DIR"),
  ACTOR("ACT");


  private String databaseType;


  HumanType(String databaseType) {
    this.databaseType = databaseType;
  }

  public String getDatabaseType() {
    return databaseType;
  }

  public static HumanType getByDatabaseType(String databaseType) {
    for (HumanType humanType : HumanType.values()) {
      if (humanType.getDatabaseType().equals(databaseType)) {
        return humanType;
      }
    }
    return null;
  }
}
