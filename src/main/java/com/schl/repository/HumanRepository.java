package com.schl.repository;

import com.schl.entity.human.Human;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;

public interface HumanRepository extends JpaRepository<Human, BigInteger> {
  Human findByName(String name);  
}
