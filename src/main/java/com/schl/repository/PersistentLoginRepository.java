package com.schl.repository;

import com.schl.entity.PersistentLogin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersistentLoginRepository extends JpaRepository<PersistentLogin, Long> {

  PersistentLogin findOneBySeries(String series);

  PersistentLogin findOneByUsername(String username);

}
