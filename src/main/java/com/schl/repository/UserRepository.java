package com.schl.repository;

import com.schl.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findByNameLike(String nameLike);

    User findFirstByNameLike(String nameLike);

    User findFirstByName(String name);

    User findFirstByNameAndPassword(String name, String password);
}
