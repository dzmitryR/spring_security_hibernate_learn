package com.schl.service;

import com.schl.entity.human.Human;

public interface HumanService {
  Human findByName(String name);
}
