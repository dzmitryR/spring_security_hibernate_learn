package com.schl.service;

import com.schl.entity.User;

public interface UserService {

    User findUser(String name);
}
