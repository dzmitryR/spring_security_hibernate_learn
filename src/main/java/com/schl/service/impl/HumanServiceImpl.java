package com.schl.service.impl;

import com.schl.entity.human.Human;
import com.schl.repository.HumanRepository;
import com.schl.service.HumanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HumanServiceImpl implements HumanService {

  private final HumanRepository humanRepository;

  @Autowired
  public HumanServiceImpl(HumanRepository humanRepository) {
    this.humanRepository = humanRepository;
  }

  @Override
  public Human findByName(String name) {
    return humanRepository.findByName(name);
  }
}
