package com.schl.service.impl;

import com.schl.entity.PersistentLogin;
import com.schl.repository.PersistentLoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class PersistentTokenServiceImpl implements PersistentTokenRepository {

  private final PersistentLoginRepository persistentLoginRepository;

  @Autowired
  public PersistentTokenServiceImpl(PersistentLoginRepository persistentLoginRepository) {
    this.persistentLoginRepository = persistentLoginRepository;
  }

  @Override
  public void createNewToken(PersistentRememberMeToken persistentRememberMeToken) {
    PersistentLogin persistentLogin = new PersistentLogin();
    persistentLogin.setUsername(persistentRememberMeToken.getUsername());
    persistentLogin.setSeries(persistentRememberMeToken.getSeries());
    persistentLogin.setToken(persistentRememberMeToken.getTokenValue());
    persistentLogin.setLastUsed(persistentRememberMeToken.getDate());

    persistentLoginRepository.saveAndFlush(persistentLogin);
  }

  @Override
  public void updateToken(String series, String tokenValue, Date lastUsed) {
    PersistentLogin persistentLogin = persistentLoginRepository.findOneBySeries(series);
    persistentLogin.setToken(tokenValue);
    persistentLogin.setLastUsed(lastUsed);
    persistentLoginRepository.saveAndFlush(persistentLogin);
  }

  @Override
  public PersistentRememberMeToken getTokenForSeries(String seriesId) {
    PersistentLogin persistentLogin = persistentLoginRepository.findOneBySeries(seriesId);
    if (persistentLogin != null) {

      return new PersistentRememberMeToken(persistentLogin.getUsername(),
          persistentLogin.getSeries(),
          persistentLogin.getToken(),
          persistentLogin.getLastUsed());
    } else {
      return null;
    }
  }

  @Override
  public void removeUserTokens(String username) {
    PersistentLogin persistentLogin = persistentLoginRepository.findOneByUsername(username);
    if (persistentLogin != null) {
      persistentLoginRepository.delete(persistentLogin);
    }
  }
}
