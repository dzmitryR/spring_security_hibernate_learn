package com.schl.service.impl;


import com.schl.entity.User;
import com.schl.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
  private final UserService userService;

  @Autowired
  public UserDetailServiceImpl(UserService userService) {
    this.userService = userService;
  }

  @Override
  public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
    User user = userService.findUser(name);
    Set<GrantedAuthority> roles = new HashSet<>();
    user.getRoles().forEach(role -> roles.add(new SimpleGrantedAuthority(role.getName())));


    UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getName(),
        user.getPassword(),
        roles);

    return userDetails;
  }
}
