<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Admin page</title>

  <link href="static/css/bootstrap.min.css" rel="stylesheet">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="static/js/bootstrap.min.js"></script>

</head>
<script type="application/javascript">
    $(document).ready(function () {
        $("#findHuman").click(function () {
            var humanName = $("#humanName").val();
            if (humanName.length > 0) {
                $.get("/human/" + humanName, function (result) {
                    if (result == '') {
                        $("#humanSearchResult").text("NOT FOUND");
                    }
                    else {
                        $("#humanSearchResult").text(JSON.stringify(result));
                    }
                });
            }
            else {
                $("#humanSearchResult").text("EMPTY HUMAN NAME!");
            }
        });
    });
</script>
<body>
<div class="container">
  <div class="row">
    <h1>Admin page</h1>
  </div>

  <div class="row">
    <div class="form-group">
      <label for="humanName">Human name:</label>
      <input type="text" class="form-control" id="humanName">
    </div>
    <button class="btn btn-default" id="findHuman">Find human</button>
  </div>
  <br>

  <div class="row">
    <div class="form-group">
      <p id="humanSearchResult"/>
    </div>
  </div>
</div>
</body>
</html>
