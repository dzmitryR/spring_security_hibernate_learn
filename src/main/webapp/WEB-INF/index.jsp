<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <meta name="_csrf" content="${_csrf.token}"/>
  <meta name="_csrf_header" content="${_csrf.headerName}"/>


  <title>Title</title>

  <link href="static/css/bootstrap.min.css" rel="stylesheet">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="static/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
  <h1>Authorized page</h1>

  <c:url value="/logout" var="logoutUrl"/>
  <form:form action="${logoutUrl}" method="post">
    <button type="submit" class="btn btn-success">Exit</button>
  </form:form>

  <a class="btn btn-success" href="/admin_page" role="button">Open only for ROLE_ADMIN</a>
</div>
</body>
</html>
