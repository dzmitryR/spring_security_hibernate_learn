<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
  <link href="static/css/bootstrap.min.css" rel="stylesheet">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="static/js/bootstrap.min.js"></script>
</head>

<body>

<div class="container">
  <h1>Login test1 Pass pass</h1>
  <h2>User test1 pass</h2>
  <h2>Admin admin admin</h2>

  <!--suppress XmlPathReference -->
  <c:url value="/j_spring_security_check" var="loginUrl"/>
  <form:form action="${loginUrl}" method="post">
    <div class="form-group">
      <label for="username">Login</label>
      <input id="username" class="form-control" type="text" name="j_username">
    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input id="password" class="form-control" type="password" name="j_password">
    </div>
    <div class="form-group">
      <div class="checkbox">
        <label>
          <input type="checkbox" name="remember-me"> Remember me
        </label>
      </div>
    </div>
    <button class="btn btn-lg btn-success" type="submit">Login</button>
  </form:form>
</div>

</body>
</html>